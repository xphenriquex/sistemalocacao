﻿using SistemaLocacao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.ViewModel
{
    public class LocacaoVM
    {
        public LocacaoVM()
        {
            Locacao = new Locacao();
            Veiculo = new Veiculo();
        }

        public Locacao Locacao;
        public Veiculo Veiculo;
        public String Message;
        public bool Success;
    }
}