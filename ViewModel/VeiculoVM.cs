﻿using SistemaLocacao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.ViewModel
{
    public class VeiculoVM
    {
        public VeiculoVM()
        {
            Veiculo = new Veiculo();
            veiculoList = new List<Veiculo>();
        }

        public Veiculo Veiculo;
        public List<Veiculo> veiculoList;
        public String Message;
        public bool Success;
    }
}