﻿using FluentNHibernate.Mapping;
using SistemaLocacao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.Mapping
{
    public class VeiculoMap : ClassMap<Veiculo>
    {
        public VeiculoMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Marca);
            Map(x => x.Modelo);
            Map(x => x.Cor);
            Map(x => x.Ano);
        }
    }
}