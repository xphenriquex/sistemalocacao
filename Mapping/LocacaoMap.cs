﻿using FluentNHibernate.Mapping;
using SistemaLocacao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.Mapping
{
    public class LocacaoMap : ClassMap<Locacao>
    {
        public LocacaoMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.IsLocado);
            Map(x => x.Diaria);
            Map(x => x.DataInicioLocacao);
            Map(x => x.DataFimLocacao);

            References(x => x.Veiculo);
        }
    }
}