﻿using SistemaLocacao.Infra;
using SistemaLocacao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.DAO
{
    public class VeiculoDAO
    {
        public List<Veiculo> GetAll()
        {
            using (var session = NhibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var result = (List<Veiculo>) session.CreateCriteria<Veiculo>().List<Veiculo>();

                    session.Close();

                    return result;
                }
            }
        }

        public void SaveOrUpdate(Veiculo Veiculo)
        {
            using ( var session = NhibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.SaveOrUpdate(Veiculo);

                    trans.Commit();
                    session.Close();
                }
            }
        }

        public Veiculo GetById(int id)
        {
            using (var session = NhibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var result = session.Get<Veiculo>(id);

                    session.Close();

                    return result;
                }
            }
        }

        public void Delete(int id)
        {
            using (var session = NhibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var result = session.Get<Veiculo>(id);

                    session.Delete(result);

                    trans.Commit();
                    session.Close();
                }
            }


        }
    }
}