﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.Models
{
    public class Locacao
    {
        public virtual int Id { get; set; }
        public virtual Veiculo Veiculo { get; set; }
        public virtual bool IsLocado { get; set; }
        public virtual int Diaria { get; set; }
        public virtual int DataInicioLocacao { get; set; }
        public virtual int DataFimLocacao { get; set; }

    }
}