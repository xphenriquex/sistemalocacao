﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaLocacao.Models
{
    public class Veiculo
    {
        public virtual int Id { get; set; }
        public virtual string Marca { get; set; }
        public virtual string Modelo { get; set; }
        public virtual int Ano { get; set; }
        public virtual string Cor { get; set; }

    }
}