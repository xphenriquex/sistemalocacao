﻿using SistemaLocacao.DAO;
using SistemaLocacao.Models;
using SistemaLocacao.ViewModel;
using System.Web.Mvc;

namespace SistemaLocacao.Controllers
{
    public class VeiculoController : Controller
    {

        private VeiculoVM _viewModel;
        private VeiculoDAO _dao;

        public VeiculoController()
        {
            _viewModel = new VeiculoVM();
            _dao = new VeiculoDAO();

            _viewModel.Message = "";
            _viewModel.Success = false;

        }

        // GET: Veiculo
        public ActionResult Index()
        {
            _viewModel.veiculoList = _dao.GetAll();

            return View(_viewModel);
        }

        public ActionResult Create()
        {
            return View(_viewModel);
        }

        public ActionResult Store(Veiculo Veiculo)
        {
            if (ValidaQuantidadeVeiculos())
            {
                _dao.SaveOrUpdate(Veiculo);

                PreencherDados("Veículo cadastrado com sucesso!");

                return View("Index", _viewModel);
            }


            PreencherDados("Apenas 5 Veículos pode ser cadastrados cadastrados!", false);

            return View("Index", _viewModel);


        }

        public ActionResult Edit(int id)
        {
            _viewModel.Veiculo = _dao.GetById(id);

            return View("Edit", _viewModel);

        }

        public ActionResult Update(Veiculo Veiculo)
        {
            _dao.SaveOrUpdate(Veiculo);

            PreencherDados("Veículo atualizado com sucesso!");

            return View("Index", _viewModel);
        }

        public ActionResult Delete(int id)
        {
            _dao.Delete(id);

            PreencherDados("Veículo deletado com sucesso!");

            return View("Index", _viewModel);
        }


       public void PreencherDados(string mensagem, bool success = true)
        {
            _viewModel.Message = mensagem;
            _viewModel.Success = success;

            _viewModel.veiculoList = _dao.GetAll();
        }

        public bool ValidaQuantidadeVeiculos()
        {
            var total = _dao.GetAll().Count;

            if(total >= 5)
            {
                return false;
            }

            return true;
        }

    }
}