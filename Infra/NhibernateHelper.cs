﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using SistemaLocacao.Models;

namespace SistemaLocacao.Infra
{
    public class NhibernateHelper
    {
        private static readonly ISessionFactory SessionFactory = CreateSessionFactory();
        private static ISessionFactory CreateSessionFactory()
        {
            var stringConnection = "Server = localhost; Port = 5432; Database = locacao; Uid = postgres; Pwd = admin;";

            IPersistenceConfigurer DBConfig = PostgreSQLConfiguration
                .PostgreSQL81
                .ConnectionString(stringConnection)
                .ShowSql();

            FluentConfiguration configuration = Fluently
                .Configure()
                .Database(DBConfig)
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Veiculo>());

            return configuration.BuildSessionFactory();
        }
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}